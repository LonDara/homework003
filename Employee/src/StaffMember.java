import java.util.Comparator;
import java.util.List;

abstract class StaffMember{
    protected  int id;
    protected  String name;
    protected  String address;
    public StaffMember(int id,String name,String address){
        this.id = id;
        this.name = name;
        this.address = address;
    }

    @Override
    public String toString() {
//        return "StaffMember{" + "id = " + id + "\n name = '" + name + '\'' + "\n address = '" + address + '\'' + '}';
        return "ID = " + id +"\nName = " + name +"\nAddress = "+address;
    }
    abstract double pay();
    public void setId(int id){
        this.id = id;
    }
    public static Comparator<StaffMember> StuNameComparator = new Comparator<StaffMember>() {

        public int compare(StaffMember s1,StaffMember s2) {
            String StudentName1 = s1.getName().toUpperCase();
            String StudentName2 = s2.getName().toUpperCase();

            //ascending order
            return StudentName1.compareTo(StudentName2);

            //descending order
            //return StudentName2.compareTo(StudentName1);
        }};
    public void setName(String name){
        this.name = name;
    }
    public void setAddress(String address){
        this.address = address;
    }
    public int getId(){
        return  this.id;
    }
    public String getName(){
        return this.name;
    }
    public String getAddress(){
        return this.address;
    }
}
