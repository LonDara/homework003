import java.util.Scanner;
/*
* @author LON DARA
* Class: KPS
* Java Developer.
* error 0.001%.
*
* */
public class MainProgram {
    public static void main(String [] args){
        Process mp = new Process();
        Scanner sc = new Scanner(System.in);
        boolean Runtime=true;
        mp.SetList();
        mp.Dispaly();
        while(Runtime==true){
            System.out.println("========================================================");
            System.out.println("1. Add New Staff Member.");
            System.out.println("2. Edite Staff Member.");
            System.out.println("3. Remove Staff Member.");
            System.out.println("4. Exit program\n");
            System.out.print("=>Choose Option(1->4) : ");
            while(!sc.hasNextInt()){
                String input = sc.next();
                System.out.print("=>Choose option(1->4) :");
            }
            int ch = sc.nextInt();
            System.out.println("========================================================");
            switch(ch){
                case 1:
                    mp.Add();
                    mp.Dispaly();
                    break;
                case 2:
                    mp.Update();
                    mp.Dispaly();
                    break;
                case 3:
                    mp.Remove();
                    mp.Dispaly();
                    break;
                case 4:
                    Runtime = false;
                    System.out.println("^-^ Bye ^-^");
                    break;
                default:
                    System.out.println("Please Enter Number (1 -> 4)");
                    break;
            }
        }
    }
}
